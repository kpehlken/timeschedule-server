const mongoose = require("mongoose");

const prioritySchema = mongoose.Schema({
    priority: {
        type: Number,
        required: true
    },
    item: {
        type: String,
        required: true
    }
}, { collection: "priority" });

module.exports = mongoose.model("Priority", prioritySchema);