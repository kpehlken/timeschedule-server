const mongoose = require("mongoose");

const scoreSchema = mongoose.Schema({
    totalScore: {
        type: Number
    },
    lastWeekSum: {
        type: Number
    },
    newWeekSum: {
        type: Number
    },
    dailyLimit: {
        type: Number
    }
}, { collection: 'score' });

module.exports = mongoose.model("Score", scoreSchema);