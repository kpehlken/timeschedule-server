const mongoose = require("mongoose");

const progressSchema = mongoose.Schema({
    progress: {
        type: Number
    },
    category: {
        type: String
    }
}, { collection: "progress" });

module.exports = mongoose.model("Progress", progressSchema);