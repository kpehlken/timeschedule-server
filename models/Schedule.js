const mongoose = require("mongoose");

const ScheduleSchema = mongoose.Schema({
    week: {
        type: String,
        required: true
    },
    weekend: {
        type: String,
        required: false
    },
    day: {
        type: Number,   // new Date().getDays()
        required: true
    },
    schedule: {
       type: Object,
       required: true
   }
});

module.exports = mongoose.model("Schedule", ScheduleSchema);