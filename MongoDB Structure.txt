

Cluster != DB

Cluster: TimeSchedule
Database: TimeSchedule

MongoDB
-------


Collections:
------------

 * Schedule (One Row representing Schedule of 1 Day)

 * Score
    (1 Row: Total Score,
    Save Sum of Score of last week,
    Save Sum of Score of weekdays until today [ Reset on Sundays ],
    Save Daily Score Limit [ Easier to update ]
    
    )

 * Progress
    (
        1 Row = 1 Category
        1 Row: Progress per Category 

    )

 * ProrityList
    (
        1 Row: 1 Thing on PaperList
    )
    