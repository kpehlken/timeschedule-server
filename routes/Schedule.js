const express = require('express');
const router = express.Router();
const Schedule = require("../models/Schedule");

router.get("/schedule", async (req, res) => {
	let schedule = new Schedule({
        week: "a",
        day: 1,
        schedule: {
            "6:00": { content: "Aufstehen", completed: false },
            "8:00": { content: "Schulbeginn", completed: false },
            "14:00": { content: "Schulende", completed: false },
            "15:30": { content: "Lesen", completed: false },
            "17:00": { content: "Fahrschule", completed: false },
            "19:00": { content: "Polnisch", completed: false },
            "20:00": { content: "(Daily)", completed: false },
            "21:00": { content: "Just fit", completed: false },
        }
    });
    schedule.save();

    let tuesdaySchedule = new Schedule({
        week: "a",
        day: 2,
        schedule: {
            "6:00": { content: "Aufstehen", completed: false },
            "8:00": { content: "Schulbeginn", completed: false },
            "16:00": { content: "Schulende", completed: false },
            "16:30": { content: "(Daily)", completed: false },
            "17:00": { content: "EMG-Plan", completed: false },
            "18:30": { content: "Fahrschule", completed: false },
            "21:00": { content: "Just fit", completed: false },
        } 
    });
    tuesdaySchedule.save();

    let wednesdaySchedule = new Schedule({
        week: "a",
        day: 3,
        schedule: {
            "6:00": { content: "Aufstehen", completed: false },
            "8:00": { content: "Schulbeginn", completed: false },
            "16:00": { content: "Schulende", completed: false },
            "17:00": { content: "Fahrschule", completed: false },
            "19:00": { content: "E-commerce", completed: false },
            "20:00": { content: "(Daily)", completed: false },
            "21:00": { content: "Just fit", completed: false },
        } 
    });
    wednesdaySchedule.save();

    let thursdaySchedule = new Schedule({
        week: "a",
        day: 4,
        schedule: {
            "6:00": { content: "Aufstehen", completed: false },
            "8:00": { content: "Schulbeginn", completed: false },
            "16:00": { content: "Schulende", completed: false },
            "17:00": { content: "(Daily) / Algorithmen", completed: false },
            "18:30": { content: "Fahrschule", completed: false },
            "21:00": { content: "Just fit", completed: false },
        } 
    });
    thursdaySchedule.save();

    let fridaySchedule = new Schedule({
        week: "a",
        day: 5,
        schedule: {
            "6:00": { content: "Aufstehen", completed: false },
            "8:00": { content: "Schulbeginn", completed: false },
            "16:00": { content: "Schulende", completed: false },
            "17:00": { content: "EMG-Plan", completed: false },
            "20:00": { content: "Gedankentext", completed: false },
            "21:00": { content: "Just fit", completed: false },
        } 
    });
    fridaySchedule.save();

    let saturdaySchedule = new Schedule({
        week: "a",
        weekend: "mom",
        day: 6,
        schedule: {
            "6:00": { content: "Aufstehen", completed: false },
            "8:00": { content: "Schulbeginn", completed: false },
            "16:00": { content: "Schulende", completed: false },
            "17:00": { content: "EMG-Plan", completed: false },
            "20:00": { content: "Gedankentext", completed: false },
            "21:00": { content: "Just fit", completed: false },
        } 
    });
    saturdaySchedule.save();

    let sundaySchedule = new Schedule({
        week: "a",
        weekend: "mom",
        day: 0,
        schedule: {
            "6:00": { content: "Aufstehen", completed: false },
            "8:00": { content: "Schulbeginn", completed: false },
            "16:00": { content: "Schulende", completed: false },
            "17:00": { content: "EMG-Plan", completed: false },
            "20:00": { content: "Gedankentext", completed: false },
            "21:00": { content: "Just fit", completed: false },
        } 
    });
    sundaySchedule.save();

    res.send("[ ADMIN ] Successfully created all A-schedules! ");
});

router.get("/today/schedule", (req, res) => {
	Schedule.find({ day: new Date().getDay() }).then(schedule =>{
		res.json(schedule);
	});
});

router.put("/update/task", async (req, res) => {
  //  try {
//	const { task } = req.body;
        	 
//    }
});

router.post("/complete/task", async (req, res) => {
    try {
        const { task } = req.body;
        const queryResult = await Schedule.find({ day: new Date().getDay() });
        
        const newSchedule = queryResult[0].schedule;
        newSchedule[task].completed = true;

        Schedule.findOneAndUpdate(queryResult, { schedule: newSchedule }, { new: true}, (e, doc) => {
            res.json({ success: true, doc }); 
        });
    } catch(err) {
        console.log(err);
        res.json({ success: false, errorMessage: "Please check Request Body for 'task' Property!" });
    }
});

router.post("/today/reset", async (req, res) => {
    const queryResult = await Schedule.find({ day: new Date().getDay() });
    
    const newSchedule = queryResult[0].schedule;
    for (const [key, value] of Object.entries(newSchedule)) {
      newSchedule[key].completed = false;
    }

    for (const [key, value] of Object.entries(newSchedule)) {
        console.log(`${key}: ${value.completed}`);
    }

    Schedule.findOneAndUpdate(queryResult, { schedule: newSchedule }, { new: true}, (e, doc) => {
        res.json({ success: true, doc }); 
    });
});

module.exports = router;
