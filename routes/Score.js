const router = require("express").Router();
const Score = require("../models/Score");

router.get("/score", async (req, res) => {
    const { totalScore } = await Score.find({});
    res.json(totalScore);
});

router.put("/totalscore", async (req, res) => {
    try {
        const { amount } = req.body;
        Score.findOneAndUpdate({}, { $inc: { totalScore: amount } }, { new: true}, (e, doc) => {
            res.json({ success: true, doc });
        });       
    } catch(err) {
        res.json({ sucess: false, errorMessage: "Please check Request Body for 'amount' Property!" });
    }

});

// Get Daily Point Limit (hit once per day)
router.get("/daily/limit", async (req, res) => {
    Score.find({}).then(result => {
        res.json(result[0].dailyLimit);
    });
});

router.put("/daily/limit", async (req, res) => {
    try {
        const { amount } = req.body;
        Score.findOneAndUpdate({}, { $set: { tttessttt: amount } }, { new: true, upsert: true }, (e, doc) => {
            console.log(e);
            res.json({ success: true, doc });
        });
    } catch(err) {
        res.json({ sucess: false, errorMessage: "Please check Request Body for 'amount' Property!" });
    }
});

// Send at 24:00 to Backend to submit today's earned points
router.put("/daily/submit", async (req, res) => {
    try {
        const { dailyPoints } = req.body;
        Score.findOneAndUpdate({}, { $inc: { lastWeekSum: dailyPoints } }, { new: true }, (err, doc) => {
            res.json({ success: true, doc });
        });
    } catch(err) {
        res.json({ success: false, errorMessage: "Please check Request Body for 'dailyPonts' Property!" });
    }
});

// Returns all metrics (hit once per hour). Mainly Score calculations
router.get("/daily/score", async (req, res) => {
    res.send("hi");
});


module.exports = router;