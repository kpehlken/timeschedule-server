const express = require("express");
const mongoose = require("mongoose");
const scheduleRoutes = require("./routes/Schedule");
const scoreRoutes = require("./routes/Score");
const cors = require("cors");

// Load .env File
require("dotenv").config();

const PORT = 5000 || process.env.PORT;

mongoose.connect(process.env.DB_URI, { useNewUrlParser: true}, () => {
	console.log("[+] Connect to MongoDB Cluster (DB: TimeSchedule)");
});
const app = express();

// Middleware
app.use(express.json());
app.use(cors());

app.use("/", scheduleRoutes);
app.use("/", scoreRoutes);


app.get("/", (req, res) => {
	res.send("Test")
});

app.listen(PORT, () => {
	console.log("Server started on Port ", PORT);
});
